
$ErrorActionPreference = 'Stop';


$packageName= 'nsis-unicode'
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/unsis/nsis-2.46.5-Unicode-setup.exe'

$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url

  silentArgs    = "/qn /norestart /l*v `"$env:TEMP\chocolatey\$($packageName)\$($packageName).MsiInstall.log`" /S"
  validExitCodes= @(0, 3010, 1641)

  softwareName  = 'Nullsoft Install System (Unicode)'
  checksum      = 'BCE16E21A2D1B04BC360345B371DA92D'
  checksumType  = 'md5'
}

Install-ChocolateyPackage @packageArgs

















